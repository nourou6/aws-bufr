$(document).on('change', ':file', function () {
    var input = $(this);
    var file = input.get(0).files ? input.get(0).files[0] : null;
    if (file !== null) {
        input.trigger('fileselect', [file]);
    }
});


$(document).ready(function () {
    var welcome = $("#welcome"),
        progress = $("#progress"),
        progressBar = $('#progress-bar'),
        output = $("#output"),
        resultLabel = $("#result-label"),
        resultTitle = $("#result-title"),
        resultContent = $("#result-content");

    $(':file').on('fileselect', function (event, file) {
        uploadFile(file);
    });

    $('[data-toggle="tooltip"]').tooltip();

    resultLabel.on('click', function () {
        reset();
    });

    var editor = ace.edit("editor");
    editor.setReadOnly(true);
    editor.setPrintMarginColumn(120);
    editor.setTheme("ace/theme/github");

    AWS.config.region = 'ap-southeast-2';
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-southeast-2:6b67576c-51b5-42ef-86fe-917c6e8e5778'
    });

    var s3Upload = new AWS.S3({
        params: {Bucket: 'aws-bufr-upload'}
    });

    var s3Download = new AWS.S3({
        params: {Bucket: 'aws-bufr-download'}
    });


    function uploadFile(file) {
        reset();
        welcome.hide();
        progress.show();
        progressBar.text('Uploading ...');

        var fileKey = getRandomFileKey();

        // console.log(file);
        // console.log(fileKey);
        s3Upload.upload({
            Key: fileKey,
            Metadata: {
                originalName: file.name
            },
            Body: file
        }, function (err, data) {
            if (err) {
                showResultLabel(false, 'Error on uploading file: ' + err.message);
                return;
            }
            // console.log('Successfully uploaded file.');
            decode(fileKey, file.name);
        });
    }

    function decode(fileKey, fileName) {
        progressBar.text('Decoding ...');
        var outputFormat = $(':radio:checked').val();
        $.ajax(
            'https://z07g0b8s50.execute-api.ap-southeast-2.amazonaws.com/dev/decode',
            {
                dataType: 'json',
                data: {
                    'object': fileKey,
                    'outputFormat': outputFormat
                },
                success: function (data) {
                    retrieveResult(data['object'], fileName);
                },
                error: function (data) {
                    progress.hide();
                    showResultLabel(false, data.responseJSON.description);
                }
            }
        );
    }

    function retrieveResult(fileKey, fileName) {
        progressBar.text('Retrieving ...');
        s3Download.getObject({Key: fileKey}, function (err, data) {
            progress.hide();
            if (err) {
                showResultLabel(false, 'Error on retrieving result: ' + err.message);
                return;
            }
            // console.log(data);
            showResultLabel(true, fileName);
            var text = data.Body.toString();
            showResult(text);
        });
    }

    function getRandomFileKey() {
        return Math.random().toString(36).slice(2) + Math.random().toString(36).slice(2);
    }

    function showResultLabel(success, content) {
        var title;
        if (success) {
            title = 'Success!';
            resultLabel.removeClass('alert-danger').addClass('alert-success');
        } else {
            title = 'Failure!';
            resultLabel.removeClass('alert-success').addClass('alert-danger');

        }
        resultTitle.text(title);
        resultContent.text(content);
        resultLabel.show();
    }

    function showResult(text) {
        if ($(':radio:checked').val().endsWith('JSON')) {
            editor.session.setMode("ace/mode/json");
        } else {
            editor.session.setMode("ace/mode/text");
        }
        editor.setValue(text);
        editor.navigateFileStart();
        welcome.hide();
        output.show();
    }

    function reset() {
        $(':file').val('');
        welcome.show();
        progress.hide();
        resultLabel.hide();
        output.hide();
        editor.setValue("");
    }

});
