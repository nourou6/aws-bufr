from __future__ import print_function

import sys
import json
import argparse
from cStringIO import StringIO

from pybufrkit import UnknownDescriptor
from pybufrkit.commands import command_lookup


def lookup(event, context):
    try:
        descriptors = event['queryStringParameters']['descriptors']
    except (TypeError, KeyError):
        return error_response(400, 'Descriptors not provided')

    parameters = {
        'master_table_number': None,
        'originating_centre': None,
        'originating_subcentre': None,
        'master_table_version': None,
        'local_table_version': None,
        'code_and_flag': True,
    }
    parameters.update(event.get('queryStringParameters', {}))
    print(parameters)

    ns = argparse.Namespace(
        tables_root_directory=None,
        master_table_number=parameters['master_table_number'],
        originating_centre=parameters['originating_centre'],
        originating_subcentre=parameters['originating_subcentre'],
        master_table_version=parameters['master_table_version'],
        local_table_version=parameters['local_table_version'],
        descriptors=descriptors,
        code_and_flag=bool(parameters['code_and_flag']),
    )

    sio = StringIO()
    sys.stdout, saved_stdout = sio, sys.stdout
    try:
        command_lookup(ns)
    except UnknownDescriptor as e:
        return error_response(400, 'Unknown descriptor: {}'.format(e))
    finally:
        sys.stdout = saved_stdout

    sio.seek(0)
    response = {
        'statusCode': 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": True,
        },
        'body': json.dumps({
            'result': sio.read()
        })
    }
    return response


def error_response(code, description):
    return {
        'statusCode': code,
        'headers': {  # enable CORS
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": True,
        },
        'body': json.dumps({
            'ok': False,
            'description': description
        })
    }


if __name__ == '__main__':
    print(lookup({'queryStringParameters': {'descriptors': '309052'}}, None))
